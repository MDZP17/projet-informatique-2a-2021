<!-- .slide: data-background-image="images/dnd.png" data-background-size="600px" class="chapter" -->

## L'API DND5eapi

%%%

<!-- .slide: data-background-image="images/dnd.png" data-background-size="300px" class="slide" -->

### Application Programming Interface

<p>Que nous dit wikipedia ?</p>
	<blockquote class="fragment" cite="https://fr.wikipedia.org/wiki/Interface_de_programmation">
	En informatique, une interface de programmation applicative
						(souvent désignée par le terme API pour application programming
						interface) est un ensemble normalisé de classes, de méthodes ou
						de fonctions qui sert de façade par laquelle un logiciel offre
						des services à d'autres logiciels.
	</blockquote>
</p>


%%%

<!-- .slide: data-background-image="images/dnd.png" data-background-size="300px" class="slide" -->
### Dit autrement

Une Brique applicative qui offre un service, et qu'on utilise sans savoir comment elle fonctionne.

2 types
- Les bibliothèques logicielles
- Les web services

Remarque: 
    -  Vous risquez d'utiliser les deux




%%%
<!-- .slide: data-background-image="images/dnd.png" data-background-size="300px" class="slide" -->

### Exemple d'une requête sur l'api 

On peut par exemple recuperer les informations sur un gobelin en allant chercher des informations ici : 
- https://www.dnd5eapi.co/api/monsters/goblin/

<div style="overflow:scroll; height:400px;">


<pre id="json"><code>{
  "index": "goblin",
  "name": "Goblin",
  "size": "Small",
  "type": "humanoid",
  "subtype": "goblinoid",
  "alignment": "neutral evil",
  "armor_class": 15,
  "hit_points": 7,
  "hit_dice": "2d6",
  "speed": {
    "walk": "30 ft."
  },
  "strength": 8,
  "dexterity": 14,
  "constitution": 10,
  "intelligence": 10,
  "wisdom": 8,
  "charisma": 8,
  "proficiencies": [
    {
      "proficiency": {
        "index": "skill-stealth",
        "name": "Skill: Stealth",
        "url": "/api/proficiencies/skill-stealth"
      },
      "value": 6
    }
  ],
  "damage_vulnerabilities": [],
  "damage_resistances": [],
  "damage_immunities": [],
  "condition_immunities": [],
  "senses": {
    "darkvision": "60 ft.",
    "passive_perception": 9
  },
  "languages": "Common, Goblin",
  "challenge_rating": 0.25,
  "xp": 50,
  "special_abilities": [
    {
      "name": "Nimble Escape",
      "desc": "The goblin can take the Disengage or Hide action as a bonus action on each of its turns."
    }
  ],
  "actions": [
    {
      "name": "Scimitar",
      "desc": "Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) slashing damage.",
      "attack_bonus": 4,
      "damage": [
        {
          "damage_type": {
            "index": "slashing",
            "name": "Slashing",
            "url": "/api/damage-types/slashing"
          },
          "damage_dice": "1d6+2"
        }
      ]
    },
    {
      "name": "Shortbow",
      "desc": "Ranged Weapon Attack: +4 to hit, range 80/320 ft., one target. Hit: 5 (1d6 + 2) piercing damage.",
      "attack_bonus": 4,
      "damage": [
        {
          "damage_type": {
            "index": "piercing",
            "name": "Piercing",
            "url": "/api/damage-types/piercing"
          },
          "damage_dice": "1d6+2"
        }
      ]
    }
  ],
  "url": "/api/monsters/goblin"
}
</code></pre>
</div>

%%%

<!-- .slide: data-background-image="images/dnd.png" data-background-size="300px" class="slide" -->

### Du JSON, vraiment ?

Oui. 

<blockquote class="fragment">JSON : JavaScript Object Notation <br/> Un autre format très courant du web avec le xml, yaml..</a></blockquote>

%%%

<!-- .slide: data-background-image="images/dnd.png" data-background-size="300px" class="slide" -->

### Documentation de l'API

Tout cela est documenté ici : http://www.dnd5eapi.co/docs/